﻿using UnityEngine;
using System.Collections;

public class SetGetClass
{
    public int _value;
    public int PropGetSet
    {
        get{return _value;}
        set{_value = value;}
    }
}

public class TestGetSet : MonoBehaviour 
{
    System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();
    long llastStartElapsedTime = 0;
    SetGetClass setgetObj = new SetGetClass();
    long lGetCost;
    long lSetCost;
    long lFieldGetCost;
    long lFiledSetCost;
	void Start () 
    {
        sw.Start();
	}
    private static readonly int _nLoopCount = 10000000;
	void Update () 
    {
        long startTime = sw.ElapsedMilliseconds;
        float lDelta = (startTime - llastStartElapsedTime);
        llastStartElapsedTime = sw.ElapsedMilliseconds;

        for(int i=0; i<_nLoopCount;i++)
        {            
            int a = setgetObj.PropGetSet;
        }
        lGetCost = sw.ElapsedMilliseconds - startTime;
        startTime = sw.ElapsedMilliseconds;

        for (int i = 0; i < _nLoopCount; i++)
        {
            setgetObj.PropGetSet = i;
        }
        lSetCost = sw.ElapsedMilliseconds - startTime;
        startTime = sw.ElapsedMilliseconds;

        for (int i = 0; i < _nLoopCount; i++)
        {
            int a = setgetObj._value;
        }
        lFieldGetCost = sw.ElapsedMilliseconds - startTime;
        startTime = sw.ElapsedMilliseconds;

        for (int i = 0; i < _nLoopCount; i++)
        {
            setgetObj._value = i;
        }
        lFiledSetCost = sw.ElapsedMilliseconds - startTime;
        startTime = sw.ElapsedMilliseconds;
	}

    void OnGUI()
    {
        GUI.Label(new Rect(0, Screen.height - 50, 200, 200), "lPropertyGetCost:" + lGetCost.ToString());
        GUI.Label(new Rect(0, Screen.height - 80, 200, 200), "lPropertySetCost:" + lSetCost.ToString());
        GUI.Label(new Rect(0, Screen.height - 110, 200, 200), "lFieldGetCost:" + lFieldGetCost.ToString());
        GUI.Label(new Rect(0, Screen.height - 140, 200, 200), "lFieldCost:" + lFiledSetCost.ToString());
    }
}
