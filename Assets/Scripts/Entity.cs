﻿using UnityEngine;
using System.Collections;
using MiniPhysic;

public class Entity 
{
    public Vector3 Pos
    {
        get
        {
            return _trs.position;
        }
        set
        {
            _trs.position = value;
        }
    }
    Transform _trs;
    mpCollisionObject _obj;
    static readonly float[] s_fRadius = new float[3] { 1, 0.75f, 0.25f};
    int _nSide;
    int _nTeamIndex;    

    public Entity(Vector3 vStart, int nSide, int nTeamIndex)
    {
        _nSide = nSide;
        _nTeamIndex = nTeamIndex;
//         Object obj = Resources.Load("Cube");
//         _trs = (GameObject.Instantiate(obj, vStart, Quaternion.identity) as GameObject).transform;
                _trs = new GameObject().transform;
        _trs.position = vStart;
        _obj = new mpCollisionObject(this, s_fRadius, (ushort)_nSide, (ushort)_nTeamIndex, OnHitOther);
        MiniPhysicWorld.Get().AddCollisionObject(_obj);
    }

    public void Tick(float fDelta)
    {
        _obj.Pos = new Vector2(Pos.x, Pos.z);
        if(_fTime2Stop > 0)
        {
            _fTime2Stop -= fDelta;
            if(_fTime2Stop < 0)
            {
                _bShouldMove = false;
            }
        }
        
        if(_bShouldMove)
        {
            Pos += (_nSide == 0 ? -1 : 1) * Vector3.forward * fDelta*3;
        }
        else
        {
            _fTime2Move += fDelta;
            if (_fTime2Move > 2)
            {
                _bShouldMove = true;
                _fTime2Move = 0;
            }
        }
    }
    bool _bShouldMove = true;
    float _fTime2Stop = 0;
    float _fTime2Move = 0;
    public void OnHitOther(bool isEnter, mpCollisionObject other, ushort nCollsionSet)
    {
        _fTime2Stop = 1;
    }
}
