﻿using UnityEngine;
using System.Collections;

//TODO:现在总会重复创建多个gameobject并且删除不了
public class GizmosMono : MonoBehaviour {
    
    private static GizmosMono s_instance = null;
    public static GizmosMono Get(){
        GameObject obj = GameObject.Find("GizmosMono");
        if (obj != null)
            s_instance = obj.GetComponent<GizmosMono>();
        else
        {
            if (s_instance == null)
            {
                GameObject gizmosTarget = new GameObject("GizmosMono");
                s_instance = gizmosTarget.AddComponent<GizmosMono>();
            }
        }
        return s_instance;
    }
    public delegate void DrawGizmosDelegate();
    DrawGizmosDelegate _gizmosCallback;
    [System.Diagnostics.Conditional("ENABLEGIZMOS")]
    public void BindGizosFunc(DrawGizmosDelegate func)
    {
        _gizmosCallback+=func;
    }
    
    void OnDrawGizmos()
    {
        if (s_instance != null && _gizmosCallback != null)
            _gizmosCallback.Invoke();
    }
    void OnDestory()
    {
         if(s_instance != null)
             GameObject.Destroy(s_instance);
        s_instance = null;
    }
}
