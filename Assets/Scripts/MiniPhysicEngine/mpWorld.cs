﻿//#define USE_MINIPHYSIC
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//cp方案:根据距离来决定重新计算更新频率,可以用sweep & prune 的优化方案~对于距离~
//grid方案
//空间分割dbvt方案
public enum ColliderGroup
{
    ENEMY,//敌人的作用强一点
    INTEAM,//己方的人作用力相对弱一点，不用立刻？
    OTHERFIRENDTEAM,
    MAX,
}
//参考bullet的大致结构,实现简单的针对1000项目优化的物理引擎
//TODO:尝试C++,并探索利用SIMD和NEON的可能性
namespace MiniPhysic
{    
    public class MiniPhysicWorld
    {
        static private MiniPhysicWorld s_instance;
        static public void SetWorld(MiniPhysicWorld world) { s_instance = world; }
        static public MiniPhysicWorld Get()
        {
//             if (s_instance == null)
//             {
//                 s_instance = new MiniPhysicWorld();
//             }
             return s_instance;
        }
        public MiniPhysicWorld()
        {
            GizmosMono.Get().BindGizosFunc(OnDrawGizmos);
        }
        public void Clear()
        {
            _collisionObjects.Clear();
            _broadphaseBrust.Clear();
            _broadphaseInterface.Clear();
            //GizmosMono.Get()._gizmosCallback -= OnDrawGizmos;
        }
        public void Tick(float fDeltaTime)
        {
            //update aabb
            DebugUtils.ProfilerBegin("Tick Physic");

            Vector2 vMinAabb = Vector2.zero;
            Vector2 vMaxAabb = Vector2.zero;
            for (int i = 0; i < _collisionObjects.Count; i++ )
            {
                _collisionObjects[i].GetAabb(out vMinAabb, out vMaxAabb);
                if (_collisionObjects[i]._bUseBrustBroadphase)
                    _broadphaseBrust.SetAabb(_collisionObjects[i].GetBroadphaseHandle(), vMinAabb, vMaxAabb);
                else
                    _broadphaseInterface.SetAabb(_collisionObjects[i].GetBroadphaseHandle(), vMinAabb, vMaxAabb);
            }

            _broadphaseInterface.CalculateOverlappingPairs();

            //TODO:是否可以在
            Dictionary<uint, mpBroadPhasePair> hashTable =
                (_broadphaseInterface as mpBroadphaseGrid)._pairCache._hashTable;
            foreach (uint hash in hashTable.Keys)
            {
                _contactSolver.Solve(hashTable[hash]);
            }

            //broad phase contact dectection
            //narrow phase contact dectection
//             lock(_contactsMap)
//             {
//                 _contactSolver.Solve(_contactsMap);
//             }
            DebugUtils.ProfilerEnd();
            //_contactsMap.GetEnumerator().
        }        
        public bool  RayTest(Ray ray, out mpCollisionObject hitObject)
        {            
            if(_broadphaseBrust.RayTest(ray, out hitObject))
            {
                return true;
            }
            return _broadphaseInterface.RayTest(ray, out hitObject);
        }
        //只检测对象的圆心坐标点是否在检测圈内，而不检测相碰
        public bool OverlapShpere(Vector3 vCenter, float fRadius, out List<mpCollisionObject> hitObjects)
        {
            return _broadphaseInterface.OverlapShpere(vCenter, fRadius, out hitObjects);
        }
        //带角度矩形检测，
        public bool OverlapRect(Vector3 vOriginPos, Quaternion vOriginRot,float fWidth, float fHeight
            , out List<mpCollisionObject> hitObjects)
        {
            return _broadphaseInterface.OverlapRect(vOriginPos, vOriginRot, fWidth, fHeight, out hitObjects);
        }
        #region reference to Bullet phy engine
        public void AddCollisionObject(mpCollisionObject collisionObject)
        {
            DebugUtils.Assert(collisionObject != null);
            _collisionObjects.Add(collisionObject);
            Vector2 vMinAabb, vMaxAabb;
            collisionObject.GetAabb(out vMinAabb, out vMaxAabb);
            if (collisionObject._bUseBrustBroadphase)
                collisionObject.SetBroadphaseHandle(_broadphaseBrust.CreateProxy(vMinAabb, vMaxAabb, collisionObject));
            else
                collisionObject.SetBroadphaseHandle(_broadphaseInterface.CreateProxy(vMinAabb, vMaxAabb, collisionObject));
        }
        public void RemoveCollsionObject(mpCollisionObject collisionObject)
        {
            DebugUtils.Assert(collisionObject != null);
            mpBroadphaseProxy bp = collisionObject.GetBroadphaseHandle();
            if (bp != null)
            {
                //_broadphaseInterface
                collisionObject.SetBroadphaseHandle(null);
                if (collisionObject._bUseBrustBroadphase)
                    _broadphaseBrust.DestoryProxy(bp);
                else
                    _broadphaseInterface.DestoryProxy(bp);
            }
            _collisionObjects.Remove(collisionObject);
        }
        protected List<mpCollisionObject> _collisionObjects = new List<mpCollisionObject>();
        protected mpBroadphaseInterface _broadphaseInterface = new mpBroadphaseGrid();
        protected mpBroadphaseInterface _broadphaseBrust = new mpBroadphaseBrust();
        //TODO:添加BrustBroadphase用于处理数量较少但是半径差距极大的物体
        //debug
        #endregion
        #region unity contacter caller
#if false
        //!called in main thread
        public void AddPair(Entity a, ObjectMono phyA, float fRadiusA, float fInvMassA,
            Entity b, ObjectMono phyB, float fRadiusB, float fInvMassB)
        {
            lock (_contactsMap)
            {
                string strHash = GetHashForContactPair(a, b);
                if (!_contactsMap.ContainsKey(strHash))
                {
                    _contactsMap.Add(strHash, new ContactInfo(a, phyA, fRadiusA, fInvMassA
                        , b, phyB, fRadiusB, fInvMassB));
                }
            }
        }
        //!called in main thread
        public void RemovePair(Entity a, Entity b)
        {
            lock (_contactsMap)
            {
                string strHash = GetHashForContactPair(a, b);
                if (_contactsMap.ContainsKey(strHash))
                {
                    _contactsMap.Remove(strHash);
                }
            }
        }  
        private Dictionary<string, ContactInfo> _contactsMap = new Dictionary<string, ContactInfo>();
        //Hash为了使得a，b满足交换律，既a,b b,a得到的hash是一致的
        //TODO:format GC大量
        static private string GetHashForContactPair(Entity a, Entity b)
        {
            return string.Format("{0},{1}", a.ID + b.ID, a.ID * b.ID);
        }
#endif
        #endregion
        //TODO:add velocity solver
        private ContactSolver _contactSolver = new ContactSolver();

        private void OnDrawGizmos()
        {
            Vector3 vStart, vEnd;
            vStart = vEnd = Vector3.zero;
            foreach(mpCollisionObject obj in _collisionObjects)
            {
                Vector2 vMinAabb, vMaxAabb;
                obj.GetAabb(out vMinAabb, out vMaxAabb);                
                vStart.x = vMinAabb.x; vStart.z = vMinAabb.y;
                vEnd.x = vMinAabb.x; vEnd.z = vMaxAabb.y;
                Gizmos.DrawLine(vStart,vEnd);
                vStart = vEnd;
                vEnd.x = vMaxAabb.x; vEnd.z = vMaxAabb.y;
                Gizmos.DrawLine(vStart, vEnd);
                vStart = vEnd;
                vEnd.x = vMaxAabb.x; vEnd.z = vMinAabb.y;
                Gizmos.DrawLine(vStart, vEnd);
                vStart = vEnd;
                vEnd.x = vMinAabb.x; vEnd.z = vMinAabb.y;
                Gizmos.DrawLine(vStart, vEnd);
                obj.OnDrawGizmos();
            }
        }
        public void OnGUI()
        {
            (_broadphaseInterface as mpBroadphaseGrid).OnGUI();

        }
    }    
}

