﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace MiniPhysic
{   
    //[StructLayout(LayoutKind.Explicit, Pack = 4)]
    public class mpBroadphaseProxy
    {
        static private uint _gid = 0;
        public mpBroadphaseProxy(Vector2 vMinAabb, Vector2 vMaxAabb, mpCollisionObject collisionObject)
        {
            _vMinAabb = vMinAabb; _vMaxAabb = vMaxAabb;
            _collisionObject = collisionObject;
            _nUniqueId = _gid++;
        }
        public Vector2 _vMinAabb;
        public Vector2 _vMaxAabb;
        public readonly mpCollisionObject _collisionObject;
        public bool _bReadyToRemove = false;    
        public uint _nUniqueId;
        public override bool Equals(object obj)
        {
            return _nUniqueId == (obj as mpBroadphaseProxy)._nUniqueId;
        }
        public override int GetHashCode()
        {
            int num = 0x17;
            return num * 0x11 + (int)_nUniqueId * 10;
        }
        public void Clean()
        {
            _bReadyToRemove = true;
        }
        //pointer to object

        //[FieldOffset(4)]
//         public byte a;
//         public byte b;

    }
    public interface mpBroadphaseInterface
    {
        //create proxy(vector3 min vector3 max filter ...)
        mpBroadphaseProxy CreateProxy(Vector2 vMinAabb, Vector2 vMaxAabb, mpCollisionObject collisionObject);
        void SetAabb(mpBroadphaseProxy proxy, Vector2 vMinAabb, Vector2 vMaxAabb);
        void CalculateOverlappingPairs();
        void DestoryProxy(mpBroadphaseProxy proxy);

        bool RayTest(Ray ray, out mpCollisionObject hitObject);
        bool OverlapShpere(Vector3 vCenter, float fRadius, out List<mpCollisionObject> hitObjects);
        bool OverlapRect(Vector3 vOriginPos, Quaternion vOriginRot, float fWidth, float fHeight
            , out List<mpCollisionObject> hitObjects);
        void Clear();
        //destory proxy

    }

}
