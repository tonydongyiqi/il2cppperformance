﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace MiniPhysic
{
    public class ContactSolver
    {
        public void Solve(mpBroadPhasePair pair)
        {
            DoSolvePosConstraint(pair);
        }
        
        /// This scale factor controls how fast overlap is resolved. Ideally this would be 1 so
        /// that overlap is removed in one time step. However using values close to 1 often lead to overshoot.        
        static readonly private float b2_baumgarte = 0.5f;//0.2f;
        /// A small length used as a collision and constraint tolerance. Usually it is
        /// chosen to be numerically significant, but visually insignificant.
        //!允许误差，box2d中是正数，我用负数以确保2个对象是彼此分开不再计算的
        static readonly private float b2_linearSlop = -0.005f;
        /// The maximum linear position correction used when solving constraints. This helps to prevent overshoot.
        static readonly private float b2_maxLinearCorrection = 0.2f;        
        private float Clamp(float a, float low, float high)
        {
            return Mathf.Max(low, Mathf.Min(a, high));
        }
        private bool DoSolvePosConstraint(mpBroadPhasePair pair)
        {
            float fSeparation = pair._fDis - pair._fSeparation;
            Entity defA = pair._proxy0._collisionObject._owner;
            Entity defB = pair._proxy1._collisionObject._owner;
            Vector3 vContactPointNormal = (defB.Pos - defA.Pos).normalized;
            float fInvMassA = pair._proxy0._collisionObject._fInvMass;
            float fInvMassB = pair._proxy1._collisionObject._fInvMass;
            float C = Clamp(b2_baumgarte * (fSeparation + b2_linearSlop), -b2_maxLinearCorrection, 0.0f);
            float K = fInvMassA + fInvMassB; //inversed mass sum
            float fImpulseValue = -C / K;
            Vector3 vImpluse = vContactPointNormal * fImpulseValue;
            //Debug.Log(string.Format("dis:{0},impluseValue:{1} ", Vector3.Distance(vPosA, vPosB), fImpulseValue));
            defA.Pos -= fInvMassA * vImpluse;
            defB.Pos += fInvMassB * vImpluse;

            return true;
        }
#if false
        private bool DoSolvePosConstraint(ContactInfo contact)
        {
            //Vector3 vPosA = contact._BodyA.GetEntityDef.Pos;
            //Vector3 vPosB = contact._BodyB.GetEntityDef.Pos;
            Vector3 vPosA = contact._PhyA._vPos;
            Vector3 vPosB = contact._PhyB._vPos;
            vPosA.y = vPosB.y = 0;
            //TODO:这个距离应该是提前算好存进来~
            float fSeparation = Vector3.Distance(vPosA, vPosB) - contact._RadiusA - contact._RadiusB;
            //             Vector3 vDelta = ;
            //             float fDis = Vector3.Distance(vPosA, vPosB);
            Vector3 vContactPointNormal = (vPosB - vPosA).normalized;
            float C = Clamp(b2_baumgarte * (fSeparation + b2_linearSlop), -b2_maxLinearCorrection, 0.0f);
            float K = contact._InvMassA + contact._InvMassB;
            float fImpulseValue = -C / K;
            Vector3 vImpluse = vContactPointNormal * fImpulseValue;
            //Debug.Log(string.Format("dis:{0},impluseValue:{1} ", Vector3.Distance(vPosA, vPosB), fImpulseValue));
            contact._BodyA.GetEntityDef.Pos -= contact._InvMassA * vImpluse;
            contact._BodyB.GetEntityDef.Pos += contact._InvMassB * vImpluse;
            //TODO:get radius
            return true;
        }
#endif
        readonly static private ushort s_PosConstraintIterations = 1;
        readonly static private ushort s_VelocityConstraintIterations = 1;
    }
}
