﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace MiniPhysic
{
    public class mpBroadphaseBrust : mpBroadphaseInterface
    {
        public mpBroadphaseProxy CreateProxy(Vector2 vMinAabb, Vector2 vMaxAabb, mpCollisionObject collisionObject)
        {
            mpBroadphaseProxy proxy =  new mpBroadphaseProxy(vMinAabb, vMaxAabb, collisionObject);
            _cachedProxy.Add(proxy);
            return proxy;
        }
        public void SetAabb(mpBroadphaseProxy proxy, Vector2 vMinAabb, Vector2 vMaxAabb) { }
        public void CalculateOverlappingPairs() { }
        public void DestoryProxy(mpBroadphaseProxy proxy) 
        {
            //int nIndex = _cachedProxy.IndexOf(proxy);
            _cachedProxy.Remove(proxy);
        }
        public bool RayTest(Ray ray, out mpCollisionObject hitObject) {
            bool bHit = false;
            hitObject = null;
            float fLambda = ray.origin.y / ray.direction.y;
            Vector3 vPointAtGround = ray.GetPoint((ray.direction * fLambda).magnitude);
            for (int i = 0; i < _cachedProxy.Count; i++ )
            {
                float fDis = Vector2.Distance(_cachedProxy[i]._collisionObject.Pos
                    , new Vector2(vPointAtGround.x, vPointAtGround.z));
                float[] fRadius = _cachedProxy[i]._collisionObject._fRadius;
                //TODO:找个最近的
                if(fDis < fRadius[0])
                {
                    hitObject = _cachedProxy[i]._collisionObject;
                    bHit = true;
                    break;
                }
            }
            return bHit; 
        }
        public bool OverlapShpere(Vector3 vCenter, float fRadius, out List<mpCollisionObject> hitObjects)
        {
            hitObjects = null;
            return false;
        }
        public bool OverlapRect(Vector3 vOriginPos, Quaternion vOriginRot, float fWidth, float fHeight
            , out List<mpCollisionObject> hitObjects)
        {
            hitObjects = null;
            return false;
        }
        public void Clear()
        {
            _cachedProxy.Clear();
        }
        List<mpBroadphaseProxy> _cachedProxy = new List<mpBroadphaseProxy>();
        //暂时只用于处理队伍的检测，所以不缓存碰撞情况
        //public mpHashOverlappingPairCache _pairCache = new mpHashOverlappingPairCache();
    }

}
