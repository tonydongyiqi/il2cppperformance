﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace MiniPhysic
{
    public class mpBroadPhasePair
    {
        public mpBroadPhasePair(mpBroadphaseProxy proxy0, mpBroadphaseProxy proxy1, ushort collsionSet)
        {
            _proxy0 = proxy0; _proxy1 = proxy1;
            _nCollisionSet = collsionSet;
            _fSeparation = proxy0._collisionObject._fRadius[collsionSet] + proxy1._collisionObject._fRadius[collsionSet];
        }
        public readonly mpBroadphaseProxy _proxy0;
        public readonly mpBroadphaseProxy _proxy1;
        public readonly ushort _nCollisionSet;// 2 敌方 1 自己队伍 0 其他队伍
        public float _fDis;
        public float _fSeparation;

    }
    
    public class mpHashOverlappingPairCache
    {
        public Dictionary<uint, mpBroadPhasePair> _hashTable = new Dictionary<uint, mpBroadPhasePair>();

        //所有产生过配对的暂时先缓存起来,空间换效率(new和GC.Collect) 可以根据距离或者其他什么东西定时清理,否则不知道会占用多少内存
        Dictionary<uint, mpBroadPhasePair> _hashTableCache = new Dictionary<uint, mpBroadPhasePair>();
        public void AddOverlappingPair(mpBroadphaseProxy proxy0, mpBroadphaseProxy proxy1, ushort collsionSet)
        {            
            //DebugUtils.ProfilerBegin("AddOverlappingPair");
            Sort(ref proxy0, ref proxy1);
            uint hash = GetHash(proxy0._nUniqueId, proxy1._nUniqueId);

            if (!_hashTable.ContainsKey(hash))
            {
                if (_hashTableCache.ContainsKey(hash))
                {
                    _hashTable.Add(hash, _hashTableCache[hash]);
                    //_hashTableCache.Remove(hash);
                }
                else
                {
                    //TODO:cache pair  create new pair when can not find old one
                    mpBroadPhasePair newPair = new mpBroadPhasePair(proxy0, proxy1, collsionSet);
                    _hashTable.Add(hash, newPair);
                    _hashTableCache.Add(hash, newPair);
                }
                //TODO:tell the phyobject ontriggerenter
                if(proxy0._collisionObject._onTriggerFunc != null)
                {
                    proxy0._collisionObject._onTriggerFunc(true, proxy1._collisionObject, collsionSet);
                }
                if(proxy1._collisionObject._onTriggerFunc != null)
                {
                    proxy1._collisionObject._onTriggerFunc(true, proxy0._collisionObject, collsionSet);
                }
                //Debug.Log("on hit");
            }

            //DebugUtils.ProfilerEnd();
        }

        //检测当前所有碰撞体之间的距离        
        public void ProcessOverlappingPair()
        {
            foreach (uint hash in _hashTable.Keys)
            {
                mpBroadPhasePair pair = _hashTable[hash];
                mpCollisionObject objA = pair._proxy0._collisionObject;
                mpCollisionObject objB = pair._proxy1._collisionObject;
                pair._fDis = Vector2.Distance(objA.Pos, objB.Pos);
//                Debug.Log(fDis + "|" + pair._nCollisionSet);
                if (pair._fDis > objA._fRadius[pair._nCollisionSet] + objB._fRadius[pair._nCollisionSet]
                    ||pair._proxy0._bReadyToRemove 
                    || pair._proxy1._bReadyToRemove)
                {
                    RemoveOverlappingPair(pair._proxy0, pair._proxy1);
                }
                //TODO:继续检测其他大小的碰撞框
            }
            DoRemoveOverlappingPair();
        }

        private List<uint> _pairToRmove = new List<uint>();
        public void RemoveOverlappingPair(mpBroadphaseProxy proxy0, mpBroadphaseProxy proxy1)
        {
            Sort(ref proxy0, ref proxy1);
            uint hash = GetHash(proxy0._nUniqueId, proxy1._nUniqueId);

            //#if ccd_enable 当速度过快时,会重复remove.(第一次时distance超出,第二次是aabb包围盒超出,在同一帧同时调用了)
            if (!_pairToRmove.Contains(hash))
                _pairToRmove.Add(hash);
        }
        private void DoRemoveOverlappingPair()
        {
            //do cleaning
            for (int i = 0; i < _pairToRmove.Count; i++)
            {
                //Debug.Log("_pairToRmove[i]:" + _pairToRmove[i]);
                mpBroadPhasePair pair = _hashTable[_pairToRmove[i]];
                mpBroadphaseProxy proxy0 = pair._proxy0;
                mpBroadphaseProxy proxy1 = pair._proxy1;

                if (proxy0._collisionObject._onTriggerFunc != null)
                {
                    proxy0._collisionObject._onTriggerFunc(false, proxy1._collisionObject, pair._nCollisionSet);
                }
                if (proxy1._collisionObject._onTriggerFunc != null)
                {
                    proxy1._collisionObject._onTriggerFunc(false, proxy0._collisionObject, pair._nCollisionSet);
                }

                _hashTable.Remove(_pairToRmove[i]);
            }
            _pairToRmove.Clear();
        }
        public void ClearAll()
        {
            _hashTable.Clear();
        }

        public int GetCollisionCount() { return _hashTable.Count; }
        public void Dump()
        {
            foreach (mpBroadPhasePair pair in _hashTable.Values)
            {
                Debug.Log(string.Format("pair:{0} {1}", pair._proxy0._nUniqueId, pair._proxy1._nUniqueId));
            }
        }
        //确保proxy0  < proxy1 以保证hashcode的正确性
        private static void Sort(ref mpBroadphaseProxy proxy0, ref mpBroadphaseProxy proxy1)
        {
            if (proxy0._nUniqueId > proxy1._nUniqueId)
            {
                mpBroadphaseProxy tmp = proxy1;
                proxy1 = proxy0;
                proxy0 = tmp;
            }
        }
        public static uint GetHash(uint proxyId1, uint proxyId2)
        {
            int key = (int)(((uint)proxyId1) | (((uint)proxyId2) << 16));
            // Thomas Wang's hash
            key += ~(key << 15);
            key ^= (key >> 10);
            key += (key << 3);
            key ^= (key >> 6);
            key += ~(key << 11);
            key ^= (key >> 16);
            return (uint)(key);
        }
    }
}