﻿using UnityEngine;
using System.Collections;

namespace MiniPhysic
{
    public class mpCollisionObject
    {
        //TODO:支持layer以便于添加队伍只用于检测的
        public mpCollisionObject(Entity entity, float[] fRaidus, ushort teamSide, ushort teamIndex, OnTrigger onTrigger, float fMass = 1)
        {
            _fRadius = fRaidus;
            _owner = entity;
            _vPos.x = entity.Pos.x;
            _vPos.y = entity.Pos.z;
            _teamSide = teamSide;
            _teamIndex = teamIndex;
            _onTriggerFunc = onTrigger;
            DebugUtils.Assert(fMass > 0);
            _fInvMass = 1 / fMass;
        }
        public delegate void OnTrigger(bool isEnter, mpCollisionObject other, ushort nCollsionSet);
        public OnTrigger _onTriggerFunc;
        public readonly Entity _owner;
        //HACK 不知道这部分逻辑怎么隔离,先做了再说.需要有个更科学的结构来支持这种奇葩的需求
        public readonly float[] _fRadius;
        public readonly ushort _teamSide;
        public readonly ushort _teamIndex;
        public bool _bIsTrigger = false;//-这个设定是参照Unity的，true则不发生碰撞 区别：连回调都不触发
        public bool _bUseBrustBroadphase = false;
        public bool _bIsSleep = false;
        public float _fInvMass;

        protected Vector2 _vPos;
        public Vector2 Pos { get { return _vPos; } set { _vPos = value; } }

        protected Vector2 _linearVelocity;
        protected mpBroadphaseProxy _broadphaseProxy;
        public void SetBroadphaseHandle(mpBroadphaseProxy proxy) { _broadphaseProxy = proxy; }
        public mpBroadphaseProxy GetBroadphaseHandle() { return _broadphaseProxy; }
        public void GetAabb(out Vector2 minAabb, out Vector2 maxAabb)
        {
            DebugUtils.Assert(_fRadius.Length > 0);
            float fMaxRadius = _fRadius[0];
            minAabb = new Vector2(_vPos.x - fMaxRadius, _vPos.y - fMaxRadius);
            maxAabb = new Vector2(_vPos.x + fMaxRadius, _vPos.y + fMaxRadius);
        }
        public void OnDrawGizmos()
        {
            for (int i = 0; i < _fRadius.Length; i++)
            {
                Gizmos.DrawWireSphere(new Vector3(_vPos.x, 0, _vPos.y), _fRadius[i]);
            }
        }
    }
}
