﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using MiniPhysic;
public class PhySicScene : MonoBehaviour {

	// Use this for initialization
    List<Entity> _entities1 = new List<Entity>();
    List<Entity> _entities2 = new List<Entity>();
    public static readonly int s_nCount = 1000;
    private static readonly int s_nCountEachRow = 50;
    private static readonly int s_nTeamCount = 100;
    MiniPhysicWorld _mpWorld;
    private static readonly float s_randomRange = 0.5f;
	void Start () {
        Application.targetFrameRate = 60;
        _mpWorld = new MiniPhysicWorld();
        MiniPhysicWorld.SetWorld(_mpWorld);        
        for(int i=0; i<s_nCount; i++)
        {
            Entity a = new Entity(new Vector3(i % s_nCountEachRow * 1.5f + Random.Range(-s_randomRange, s_randomRange)
                , 0, i / s_nCountEachRow * 1.5f + 3), 0, i / s_nTeamCount);
            _entities1.Add(a);
            Entity b = new Entity(new Vector3(i % s_nCountEachRow * 1.5f + Random.Range(-s_randomRange, s_randomRange)
                , 0, -1 * i / s_nCountEachRow * 1.5f - 3), 1, i / s_nTeamCount);
            _entities2.Add(b);
        }
	}
	
	// Update is called once per frame
	void Update () {
        float fDeltaTime = Time.deltaTime;
	    for(int i=0; i<_entities1.Count; i++)
        {
            _entities1[i].Tick(fDeltaTime);
            _entities2[i].Tick(fDeltaTime);
        }
        _mpWorld.Tick(fDeltaTime);
	}
}
